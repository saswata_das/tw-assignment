import 'package:flutter/material.dart';
import 'package:tw_assignment/helpers/league_render_check.dart';
import 'package:tw_assignment/screens/leagues.dart';
import 'package:tw_assignment/theme_data.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';

class HomeCountryButton extends StatelessWidget {
  final String countryName;
  const HomeCountryButton({Key? key, required this.countryName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: GestureDetector(
        onTap: () {
          LeagueRenderCheck().setStateRefresh();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => LeagueScreen(
                        countryName: countryName,
                      )));
        },
        child: Container(
          width: double.infinity,
          height: 48,
          decoration: BoxDecoration(
            color: Color(int.parse(secondaryRed)),
            borderRadius: const BorderRadius.all(
              Radius.circular(4.0),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  countryName,
                  style: TextStyle(
                      color: Color(int.parse(primaryBlack)),
                      fontSize: 20,
                      fontWeight: FontWeight.w700),
                ),
                const ImageIcon(Svg("assets/images/arrow_right.svg"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
