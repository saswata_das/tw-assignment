import 'package:flutter/material.dart';
import 'package:tw_assignment/theme_data.dart';

class LeagueSearch extends StatelessWidget {
  final Function onChanged;
  const LeagueSearch({Key? key, required this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 16, right: 0, bottom: 8),
      child: SizedBox(
        width: double.infinity,
        child: TextField(
            onChanged: (value) => onChanged(value),
            decoration: InputDecoration(
              hintText: 'Search leagues...',
              fillColor: Color(int.parse(secondaryWhite)),
              filled: true,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(color: Colors.white)),
              disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(color: Colors.white)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(color: Colors.white)),
            )),
      ),
    );
  }
}
