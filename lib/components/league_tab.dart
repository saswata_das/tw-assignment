import 'package:flutter/material.dart';
import 'package:tw_assignment/core/types/league_tab_data.type.dart';
import 'package:tw_assignment/theme_data.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';

class LeagueTab extends StatelessWidget {
  final LeagueTabData leagueTabData;
  const LeagueTab({Key? key, required this.leagueTabData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 112,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(
              leagueTabData.value.fold(
                  (l) => 'https://www.thesportsdb.com/images/sports/soccer.jpg',
                  (r) => r.strSportThumb),
            )),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    leagueTabData.value.fold((l) => 'N/A', (r) => r.strLeague),
                    style: TextStyle(
                        color: Color(int.parse(primaryWhite)),
                        fontWeight: FontWeight.w700,
                        fontSize: 18),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  LimitedBox(
                    maxHeight: 52,
                    maxWidth: 104,
                    child:
                        leagueTabData.value.fold((l) => const SizedBox(), (r) {
                      if (r.strLogo.isNotEmpty) {
                        return CachedNetworkImage(
                            imageUrl: r.strLogo,
                            placeholder: (context, url) =>
                                const CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error));
                      } else {
                        return const SizedBox();
                      }
                    }),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Row(
                children: [
                  leagueTabData.value.fold((l) => const SizedBox(), (r) {
                    if (r.strTwitter.isNotEmpty) {
                      return GestureDetector(
                        onTap: () {
                          launch('https://${r.strTwitter}');
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 4),
                          child: ImageIcon(
                            Svg('assets/images/twitter.svg',
                                color: Color(int.parse(primaryWhite))),
                            color: Color(int.parse(primaryWhite)),
                          ),
                        ),
                      );
                    } else {
                      return const SizedBox();
                    }
                  }),
                  leagueTabData.value.fold((l) => const SizedBox(), (r) {
                    if (r.strFacebook.isNotEmpty) {
                      return GestureDetector(
                        onTap: () {
                          launch('https://${r.strFacebook}');
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: ImageIcon(
                            Svg('assets/images/facebook.svg',
                                color: Color(int.parse(primaryWhite))),
                            color: Color(int.parse(primaryWhite)),
                          ),
                        ),
                      );
                    } else {
                      return const SizedBox();
                    }
                  }),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
