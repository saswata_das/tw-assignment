import 'package:tw_assignment/core/dao/leagues_list.dao.dart';
import 'package:tw_assignment/core/dto/league_list.dto.dart';
import 'package:tw_assignment/core/types/league_item.type%20.dart';
import 'package:tw_assignment/core/types/league_tab_data.type.dart';
import 'package:dio/dio.dart';

class LeaguesListRepo extends LeaguesList {
  final String countryName;
  LeaguesListRepo({required this.countryName});

  @override
  Future<List<LeagueTabData>> fetchAllByCountry() async {
    try {
      return toLeagueItem(await Dio().get(
              'https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=$countryName'))
          .fold(
              (l) => [],
              (r) async =>
                  toLeagueTabData(r, await _fetchBackgroundImageBySports()));
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<Map<String, String>> _fetchBackgroundImageBySports() async {
    try {
      return toLeagueThumbUrl(await Dio()
          .get('https://www.thesportsdb.com/api/v1/json/2/all_sports.php'));
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<List<LeagueTabData>> searchBySports(String sport) async {
    try {
      return toLeagueItem(await Dio().get(
              'https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=$countryName&s=$sport'))
          .fold(
              (l) => [],
              (r) async =>
                  toLeagueTabData(r, await _fetchBackgroundImageBySports()));
    } catch (e) {
      throw Exception(e);
    }
  }
}
