import 'package:flutter/material.dart';
import 'package:tw_assignment/components/home_country_button.dart';
import 'package:tw_assignment/helpers/home_country_buttons_list.dart';
import 'package:tw_assignment/theme_data.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);
  final countryList = homeCountryButtonList();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Color(int.parse(primaryRed)),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Center(
                child: ListView(
              shrinkWrap: true,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 104),
                  child: Text(
                    'The Sports DB',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(int.parse(primaryWhite)),
                        fontWeight: FontWeight.w700,
                        fontSize: 40),
                  ),
                ),
                ...countryList.map((country) => HomeCountryButton(
                      countryName: country,
                    )),
              ],
            )),
          ),
        ),
      ),
    );
  }
}
