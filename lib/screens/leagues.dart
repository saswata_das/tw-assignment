import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:tw_assignment/components/league_tab.dart';
import 'package:tw_assignment/components/league_search.dart';
import 'package:tw_assignment/controllers/league_controller.dart';
import 'package:tw_assignment/core/types/league_tab_data.type.dart';
import 'package:tw_assignment/helpers/league_render_check.dart';
import 'package:tw_assignment/theme_data.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';

class LeagueScreen extends ConsumerWidget {
  final String countryName;
  LeagueScreen({Key? key, required this.countryName}) : super(key: key);
  late final _leagueController = LeagueController(countryName: countryName);
  static const maxInt = 9223372036854775807;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    LeagueRenderCheck().init(ref, _leagueController);
    final List<LeagueTabData> leagueTabList =
        ref.watch(_leagueController.leagueListProvider.state).state;
    return MaterialApp(
      home: SafeArea(
          child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryRed)),
          title: Text(countryName,
              style: TextStyle(color: Color(int.parse(primaryWhite)))),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const ImageIcon(Svg("assets/images/arrow_left.svg")),
          ),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: ListView(
              children: [
                LeagueSearch(onChanged: (value) {
                  _leagueController.onSearchChanged(value, ref);
                }),
                if (!LeagueRenderCheck().hasListLoadedOnce() &&
                    leagueTabList.isEmpty) ...[
                  const Center(
                    child: CircularProgressIndicator(),
                  )
                ] else if (leagueTabList.isEmpty) ...[
                  const Center(
                    child: Text('No matches found or Loading'),
                  )
                ] else ...[
                  ...leagueTabList.map((e) => LeagueTab(
                        leagueTabData: e,
                      ))
                ],
              ],
            ),
          ),
        ),
      )),
    );
  }
}
