import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:tw_assignment/screens/home.dart';

void main() {
  runApp(ProviderScope(child: HomeScreen()));
}
