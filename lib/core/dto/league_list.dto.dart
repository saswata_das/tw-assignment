import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:tw_assignment/core/types/league_item.type%20.dart';
import 'package:tw_assignment/core/types/league_tab_data.type.dart';
import 'package:tw_assignment/core/types/utils.type.dart';

Either<bool, List<LeagueItem>> toLeagueItem(dynamic value) {
  Map<String, dynamic> leagueListMap = jsonDecode(value.toString());
  List? leaguesList = leagueListMap['countrys'];
  if (leaguesList != null) {
    return Right(leaguesList
        .map((e) => LeagueItem(e['idLeague'], e['strLeague'], e['strSport'],
            e['strFacebook'], e['strTwitter'], e['strLogo']))
        .toList());
  } else {
    return const Left(false);
  }
}

Map<String, String> toLeagueThumbUrl(dynamic value) {
  Map<String, dynamic> sportsListMap = jsonDecode(value.toString());
  List sportsList = sportsListMap['sports'];
  return {
    for (var item in sportsList)
      item['strSport'].toString(): item['strSportThumb'].toString()
  };
}

List<LeagueTabData> toLeagueTabData(
    List<LeagueItem> leagueItems, Map<String, String> sportThumbs) {
  return leagueItems
      .map((e) => LeagueTabData(
          idLeague: e.idLeague,
          strLeague: e.strLeague,
          strSport: e.strSport,
          strFacebook: validateString(e.strFacebook),
          strTwitter: validateString(e.strTwitter),
          strLogo: validateString(e.strLogo),
          strSportThumb: validateString(sportThumbs[e.strSport])))
      .toList();
}
