import 'package:dartz/dartz.dart';
import 'package:tw_assignment/core/types/utils.type.dart';

class LeagueTabData {
  factory LeagueTabData(
      {required String idLeague,
      required String strLeague,
      required String strSport,
      required String strFacebook,
      required String strTwitter,
      required String strLogo,
      required String strSportThumb}) {
    if (isAnyStringNullOrEmpty(
        [idLeague, strLeague, strSport, strSportThumb])) {
      return LeagueTabData._(left(throw Exception(
          'sports id or name or type or background image cannot be empty')));
    }
    return LeagueTabData._(right(_ItemRecord(idLeague, strLeague, strSport,
        strFacebook, strTwitter, strLogo, strSportThumb)));
  }

  const LeagueTabData._(this.value);

  final Either<Exception, _ItemRecord> value;
}

class _ItemRecord {
  final String idLeague;
  final String strLeague;
  final String strSport;
  final String strFacebook;
  final String strTwitter;
  final String strLogo;
  final String strSportThumb;

  const _ItemRecord(this.idLeague, this.strLeague, this.strSport,
      this.strFacebook, this.strTwitter, this.strLogo, this.strSportThumb);
}
