bool isNullOrEmptyString(String? value) {
  if (value == null || value.isEmpty) {
    return true;
  } else {
    return false;
  }
}

bool isAnyStringNullOrEmpty(List<String?> value) {
  for (var element in value) {
    if (isNullOrEmptyString(element)) {
      return true;
    }
  }
  return false;
}

String validateString(String? value) {
  if (value == null || value.isEmpty) {
    return '';
  } else {
    return value;
  }
}
