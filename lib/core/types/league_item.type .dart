class LeagueItem {
  final String idLeague;
  final String strLeague;
  final String strSport;
  final String? strFacebook;
  final String? strTwitter;
  final String? strLogo;

  const LeagueItem(this.idLeague, this.strLeague, this.strSport,
      this.strFacebook, this.strTwitter, this.strLogo);
}
