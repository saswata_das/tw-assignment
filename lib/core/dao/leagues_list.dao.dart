import 'package:tw_assignment/core/types/league_tab_data.type.dart';

abstract class LeaguesList {
  Future<List<LeagueTabData>> fetchAllByCountry();
  Future<List<LeagueTabData>> searchBySports(String sport);
}
