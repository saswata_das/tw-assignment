import 'package:flutter/rendering.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:tw_assignment/controllers/league_controller.dart';

class LeagueRenderCheck {
  bool _isRendered = false;
  bool _isListLoadedFirstTime = false;
  static final LeagueRenderCheck _instance = LeagueRenderCheck._internal();
  factory LeagueRenderCheck() {
    return _instance;
  }
  LeagueRenderCheck._internal();

  init(WidgetRef ref, LeagueController leagueController) {
    if (!_isRendered) {
      leagueController.fetchLeagues(ref);
      _setLeagueRendered();
    }
  }

  _setLeagueRendered() {
    _isRendered = true;
  }

  setStateRefresh() {
    _isRendered = false;
    _isListLoadedFirstTime = false;
  }

  _setListLoadedOnce() async {
    await Future.delayed(const Duration(seconds: 1));
    _isListLoadedFirstTime = true;
  }

  bool hasListLoadedOnce() {
    _setListLoadedOnce();
    return _isListLoadedFirstTime;
  }
}
