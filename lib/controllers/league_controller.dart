import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:tw_assignment/core/types/league_tab_data.type.dart';
import 'package:tw_assignment/repos/leagues_list_repo.dart';

class LeagueController {
  final String countryName;
  late final _leagueListRepo = LeaguesListRepo(countryName: countryName);

  LeagueController({required this.countryName});

  final leagueListProvider = StateProvider<List<LeagueTabData>>((ref) {
    return [];
  });

  fetchLeagues(WidgetRef ref) async {
    ref.read(leagueListProvider.state).state = [
      ...await _leagueListRepo.fetchAllByCountry()
    ];
  }

  onSearchChanged(String sport, WidgetRef ref) {
    if (sport.isEmpty) {
      fetchLeagues(ref);
    } else {
      _searchLeaguesBySports(sport, ref);
    }
  }

  _searchLeaguesBySports(String sport, WidgetRef ref) async {
    ref.read(leagueListProvider.state).state = [
      ...await _leagueListRepo.searchBySports(sport)
    ];
  }
}
